import Vue from "vue";
import VueRouter from "vue-router";
import Add from "../views/Add.vue";

Vue.use(VueRouter);
const routes = [
  {
    path: "/",
    name: "Add",
    component: Add,
  },
  {
    path: "/ver",
    name: "ver",
    component: () => import("../views/Ver.vue"),
  },
];
const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
