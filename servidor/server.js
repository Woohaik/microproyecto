const app = require("express")();
const path = require("path");
const history = require("connect-history-api-fallback");
//Para que al igual que vue reconozca una ruta como "/ver" desde el comienzo ya que es una sola pagina la aplicacion
app.use(history());
app.use(require("express").static(path.join(__dirname, "public")));
//El puero que se va a usar
const PORT = 5000;
app.listen(PORT, () => console.log(`Aplicacion en el puerto ${PORT}`));
